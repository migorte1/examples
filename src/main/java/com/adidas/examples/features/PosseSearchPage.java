package com.adidas.examples.features;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

@DefaultUrl("http://posse.com")
public class PosseSearchPage extends PageObject {

    @FindBy(className = "searchTermInput")
    private WebElement termSearchBar;

    @FindBy(className = "searchLocationInput")
    private WebElement locationSearchBar;

    public PosseSearchPage(WebDriver driver) {
        super(driver);
    }

    public PosseResultsPage lookForLunchPlacesWithKeywords(String term, String location){
        element(termSearchBar).sendKeys(term);
        element(locationSearchBar).sendKeys(location, Keys.ENTER);

        return new PosseResultsPage(getDriver());
    }
}
