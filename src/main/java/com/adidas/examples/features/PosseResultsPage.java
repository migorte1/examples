package com.adidas.examples.features;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class PosseResultsPage extends PageObject {

    @FindBy(className = "placeName")
    private List<WebElement> lunchPlacesList;

    @FindBy(css = ".leftContentFooter > span > span")
    private WebElement resultSum;

    public PosseResultsPage(WebDriver driver) {
        super(driver);
    }

    public int getSum() {
        new WebDriverWait(getDriver(), 60).until(ExpectedConditions.visibilityOf(resultSum));
        return Integer.valueOf(resultSum.getText());
    }

    public String getFirstResult() {
        assert !lunchPlacesList.isEmpty();
        return lunchPlacesList.get(0).getText();
    }
}
