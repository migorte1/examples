package com.adidas.examples.features;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@DefaultUrl("https://www.ryanair.com/us/en/")
public class RyanairHomePage extends PageObject {

    /*******************************************
     * ELEMENTOS PARA INTRODUCIR LOS AEROPUERTOS
     ******************************************/
    @FindBy(xpath = "//div[@class='col-departure-airport']//input[@placeholder='Departure airport']")
    private WebElement inputDepartureAirport;

    @FindBy(xpath = "//div[@class='col-destination-airport']//input[@placeholder='Destination airport']")
    private WebElement inputArrivalAirport;

    @FindBy(css = "form[name=\"formFlightSearch\"] button:nth-child(1)")
    private WebElement buttonContinueAirports;

    /************************************
     * ELEMENTOS PARA INTRODUCIR LA FECHA
     ***********************************/
    @FindBy(css = "div#row-dates-pax div.container-from > div > div.ng-pristine.ng-untouched.ng-empty.ng-invalid.ng-invalid-required > label")
    private WebElement inputOut;

    @FindBy(xpath = "//div[@class='col-cal-from']//input[@placeholder='DD']")
    private WebElement inputDDOut;

    @FindBy(xpath = "//div[@class='col-cal-from']//input[@placeholder='MM']")
    private WebElement inputMMOut;

    @FindBy(xpath = "//div[@class='col-cal-from']//input[@placeholder='YYYY']")
    private WebElement inputYYYYOut;

    @FindBy(xpath = "//div[@class='col-cal-to']//input[@placeholder='DD']")
    private WebElement inputDDBack;

    @FindBy(xpath = "//div[@class='col-cal-to']//input[@placeholder='MM']")
    private WebElement inputMMBack;

    @FindBy(xpath = "//div[@class='col-cal-to']//input[@placeholder='YYYY']")
    private WebElement inputYYYYBack;

    @FindBy(className = "arrow right")
    private WebElement buttonNextMonth;

    /******************************************
     *  ELEMENTOS PARA INTRODUCIR LOS PASAJEROS
     *****************************************/
    @FindBy(className = "close-icon")
    private WebElement closeIcon;

    @FindBy(xpath = "//div[@aria-label='1 Passenger']")
    private WebElement inputPassengers;

    @FindBy(xpath = "//div[@label='Adults']//button[@class='core-btn inc core-btn-wrap']")
    private WebElement buttonIncAdults;

    @FindBy(xpath = "//div[@label='Children']//button[@class='core-btn inc core-btn-wrap']")
    private WebElement buttonIncChildren;

    @FindBy(className = "datepicker-wrapper r scrollable six-rows")
    private WebElement datePicker;

    @FindBy(xpath = "//div[@id='search-container']/div/div/form/div[3]/button[2]")
    private WebElement buttonContinue;

    @FindBy(xpath = "//button[@class='core-btn-primary core-btn-phone-full']")
    private WebElement buttonBanner;

    private String[] months = new String[]{"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

    public RyanairHomePage(WebDriver driver) {
        super(driver);
    }

    /**
     * Picks the departure airport and the arrival airport.
     *
     * @param from Departure airport.
     * @param to   Arrival airport.
     */
    public void pickAirports(String from, String to) {
        JavascriptExecutor jsExecuter = (JavascriptExecutor) getDriver();

        element(inputDepartureAirport).click();
        element(inputDepartureAirport).typeAndTab(from);
        element(inputArrivalAirport).click();
        element(inputArrivalAirport).typeAndEnter(to);

        element(buttonContinueAirports).click();
        element(buttonContinueAirports).click();

        try {
            WebElement buttonOK = find(By.xpath("//*[starts-with(@id, 'ngdialog')]//dialog-body/div/button"));

            getDriver().manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

            jsExecuter.executeScript("arguments[0].click();", buttonOK);

        } catch (Exception e) {
            System.out.println("There is no popup element to close");
        }
    }

    /**
     * Picks the date of the flight out and the date of the flight back.
     *
     * @param out  Date of the flight out.
     * @param back Date of the flight back.
     */
    public void pickFlyOutAndFlyBack(Date out, Date back) {
        new WebDriverWait(getDriver(), 60).until(ExpectedConditions.visibilityOf(inputOut));
        element(inputOut).click();

        Calendar calOut = Calendar.getInstance();
        calOut.setTime(out);
        Calendar calBack = Calendar.getInstance();
        calBack.setTime(back);

        /*****************************************************************************************************
         * INTRODUCIR FECHA CLICANDO EN EL CALENDARIO
         ****************************************************************************************************/

        String monthToSearch = months[calOut.get(Calendar.MONTH)] + " " + calOut.get(Calendar.YEAR);

        JavascriptExecutor jsExecuter = (JavascriptExecutor) getDriver();

        WebElement monthName = find(By.className("month-name"));
        java.util.List<WebElement> list = findCurrentMonthsAtCalendar();
        new WebDriverWait(getDriver(), 60).until(ExpectedConditions.visibilityOf(monthName));

        boolean flag;

        flag = ifListContainsItem(list, monthToSearch);

        while (!flag) {
            WebElement arrowRight = find(By.cssSelector(".arrow.right"));
            jsExecuter.executeScript("arguments[0].click();", arrowRight);

            list = findCurrentMonthsAtCalendar();
            flag = ifListContainsItem(list, monthToSearch);
        }

        WebElement content = find(By.className("content"));

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

        WebElement outDay = getDriver().findElement(By.xpath("//li[@data-id='" + simpleDateFormat.format(out) + "']//span"));
        jsExecuter.executeScript("arguments[0].click();", outDay);

        new WebDriverWait(getDriver(), 60).until(ExpectedConditions.visibilityOf(content));

        WebElement backDay = getDriver().findElement(By.xpath("//li[@data-id='" + simpleDateFormat.format(back) + "']//span"));
        jsExecuter.executeScript("arguments[0].click();", backDay);
    }

    /**
     * Picks the number of passengers, clicks enter to process the request and closes the banner.
     *
     * @param numAdults    Number of adults.
     * @param numChildrens Number of childrens.
     */
    public void pickNumPassengers(int numAdults, int numChildrens) {

        Boolean isPresent = getDriver().findElements(By.className("close-icon")).size() > 0;

        if (isPresent) {
            try {
                element(closeIcon).click();
            } catch (Exception e) {
                System.out.println("There is no cookies policy banner to close");
            }
        }
        element(inputPassengers).click();

        new WebDriverWait(getDriver(), 60).until(ExpectedConditions.elementToBeClickable(buttonIncAdults));

        for (int i = 0; i < numAdults - 1; i++) {
            element(buttonIncAdults).click();
        }
        for (int i = 0; i < numChildrens; i++) {
            element(buttonIncChildren).click();
        }
    }

    /**
     * Clicks the continue button to process the request.
     *
     * @return The booking page.
     */
    public RyanairBookingPage clickContinue() {
        new WebDriverWait(getDriver(), 60).until(ExpectedConditions.elementToBeClickable(buttonContinue));
        element(buttonContinue).click();

        try{
            if (element(buttonBanner).isDisplayed())
                clickBanner();
        } catch (Exception e){
            System.out.println("There is no sales banner to close");
        }
        return new RyanairBookingPage(getDriver());
    }

    private void clickBanner() {
        new WebDriverWait(getDriver(), 60).until(ExpectedConditions.visibilityOf(buttonBanner));
        element(buttonBanner).click();
    }

    private boolean ifListContainsItem(java.util.List<WebElement> list, String item) {
        for (WebElement we : list) {
            if (we.getText().equals(item)) return true;
        }
        return false;
    }

    private java.util.List<WebElement> findCurrentMonthsAtCalendar() {
        java.util.List<WebElement> list = new ArrayList<>(findAll(By.className("month-name")));
        java.util.List<WebElement> result = new ArrayList<>();
        for (WebElement webElement : list) {
            if (webElement.isDisplayed()) result.add(webElement);
        }
        return result;
    }
}
