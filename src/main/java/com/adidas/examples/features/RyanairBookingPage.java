package com.adidas.examples.features;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.At;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

@At("https://www.ryanair.com/us/en/booking/home")
public class RyanairBookingPage extends PageObject {

    @FindBy(xpath = "//flight-list[@id='outbound']//div[@class='meta-row time']/span[1]")
    private WebElement departureTimeOut;

    @FindBy(xpath = "//flight-list[@id='inbound']//div[@class='meta-row time']/span[1]")
    private WebElement departureTimeBack;

    public RyanairBookingPage(WebDriver driver){
        super(driver);
    }

    public String getDepartureTimeOut() {
        waitForAngularRequestsToFinish();
        new WebDriverWait(getDriver(), 60).until(ExpectedConditions.visibilityOf(departureTimeOut));
        return departureTimeOut.getText();
    }

    public String getDepartureTimeBack() {
        waitForAngularRequestsToFinish();
        new WebDriverWait(getDriver(), 60).until(ExpectedConditions.visibilityOf(departureTimeBack));
        return departureTimeBack.getText();
    }
}
