package com.adidas.examples.features.testRunner;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = { "src/test/resources/features/lunchPlaces" },
        glue = { "com.adidas.examples.features.stepDefinitions.lunchPlaces" }
)
public class LunchPlacesRunner {
}
