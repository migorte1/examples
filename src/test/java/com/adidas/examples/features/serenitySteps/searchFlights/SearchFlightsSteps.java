package com.adidas.examples.features.serenitySteps.searchFlights;

import com.adidas.examples.features.RyanairBookingPage;
import com.adidas.examples.features.RyanairHomePage;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

import java.util.Date;

public class SearchFlightsSteps {

    RyanairHomePage ryanairHomePage;
    RyanairBookingPage ryanairBookingPage;

    @Step
    public void open() {
        ryanairHomePage.open();
    }

    @Step
    public void pickAirports(String from, String to) {
        ryanairHomePage.pickAirports(from, to);
    }

    @Step
    public void pickFlyOutAndFlyBack(Date out, Date back){
        ryanairHomePage.pickFlyOutAndFlyBack(out, back);
    }

    @Step
    public void pickNumPassengers(int numAdults, int numChildrens){
        ryanairHomePage.pickNumPassengers(numAdults, numChildrens);
        ryanairBookingPage = ryanairHomePage.clickContinue();
    }

    @Step
    public void checkFlightOut(String time) {
        Assert.assertTrue(ryanairBookingPage.getDepartureTimeOut().equals(time));
    }

    @Step
    public void checkFlightBack(String time) {
        Assert.assertTrue(ryanairBookingPage.getDepartureTimeBack().equals(time));
    }
}
