package com.adidas.examples.features.serenitySteps.lunchPlaces;

import com.adidas.examples.features.PosseResultsPage;
import com.adidas.examples.features.PosseSearchPage;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

public class LunchPlacesSearchSteps {

    PosseSearchPage posseSearchPage;
    PosseResultsPage posseResultsPage;

    @Step
    public void open() {
        posseSearchPage.open();
    }

    @Step
    public void searchFor(String term, String location) {
        posseResultsPage = posseSearchPage.lookForLunchPlacesWithKeywords(term, location);
    }

    @Step
    public void checkResultsSum(int sum) {
        Assert.assertTrue(posseResultsPage.getSum()==sum);
    }

    @Step
    public void checkFirstResult(String name) {
        Assert.assertTrue(posseResultsPage.getFirstResult().equals(name));
    }
}
