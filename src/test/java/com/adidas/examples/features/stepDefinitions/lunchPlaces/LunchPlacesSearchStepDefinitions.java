package com.adidas.examples.features.stepDefinitions.lunchPlaces;

import com.adidas.examples.features.serenitySteps.lunchPlaces.LunchPlacesSearchSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class LunchPlacesSearchStepDefinitions {

    @Steps
    LunchPlacesSearchSteps lunchPlacesSearchSteps;

    @Given("^I am on the homepage$")
    public void i_am_on_the_homepage() throws Throwable {
        lunchPlacesSearchSteps.open();
    }

    @When("^I search for \"([^\"]*)\" in \"([^\"]*)\"$")
    public void i_search_for_in(String term, String location) throws Throwable {
        lunchPlacesSearchSteps.searchFor(term, location);
    }

    @Then("^I should see (\\d+) results in results page$")
    public void i_should_see_results_in_results_page(int arg1) throws Throwable {
        lunchPlacesSearchSteps.checkResultsSum(arg1);
    }

    @Then("^The first element should be \"([^\"]*)\"$")
    public void the_first_element_should_be(String arg1) throws Throwable {
        lunchPlacesSearchSteps.checkFirstResult(arg1);
    }
}
