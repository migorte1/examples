package com.adidas.examples.features.stepDefinitions.searchFlights;

import com.adidas.examples.features.serenitySteps.searchFlights.SearchFlightsSteps;
import cucumber.api.Format;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

import java.util.Date;

public class SearchFlightsStepDefinitions {

    @Steps
    SearchFlightsSteps searchFlightsSteps;

    @Given("^I am on the homepage$")
    public void i_am_on_the_homepage() throws Throwable {
        searchFlightsSteps.open();
    }

    @When("^I search for a flight from \"([^\"]*)\" to \"([^\"]*)\"$")
    public void i_search_for_a_flight_from_to(String arg1, String arg2) throws Throwable {
        searchFlightsSteps.pickAirports(arg1, arg2);
    }

    @When("^The fly out is on (.+) and the fly back is on (.+)$")
    public void with_the_fly_out_on_and_the_fly_back_on(@Format("dd/MM/yyyy") Date from, @Format("dd/MM/yyyy") Date to) throws Throwable {
        searchFlightsSteps.pickFlyOutAndFlyBack(from, to);
    }

    @When("^The number of passengers is (\\d+) adults and (\\d+) children$")
    public void for_passenger(int arg1, int arg2) throws Throwable {
        searchFlightsSteps.pickNumPassengers(arg1, arg2);
    }

    @Then("^There should be a flight out with departure time \"([^\"]*)\"$")
    public void there_should_be_a_flight_out_with_departure_time(String arg1) throws Throwable {
        searchFlightsSteps.checkFlightOut(arg1);
    }

    @Then("^A flight back with departure time \"([^\"]*)\"$")
    public void a_flight_back_with_departure_time(String arg1) throws Throwable {
        searchFlightsSteps.checkFlightBack(arg1);
    }
}
