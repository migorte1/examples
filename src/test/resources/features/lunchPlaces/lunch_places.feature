# https://posse.com/
Feature: Know information from lunch places
  In order to know where to lunch
  As a generic user
  I want to be able to search for lunch places

  Scenario: Get lunch place info
    Given I am on the homepage
    When I search for "Lunch" in "Madrid"
    Then I should see 4 results in results page
      And The first element should be "Mercado de San Miguel"
