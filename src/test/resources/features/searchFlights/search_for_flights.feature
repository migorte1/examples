#ryanair.com
Feature: Search flights
  In order to know which flights are
  As a generic user
  I want to be able to search for flights between two cities

  Scenario Outline: Get flights info
    Given I am on the homepage
    When I search for a flight from "<departure_airport>" to "<arrival_airport>"
    And The fly out is on <fly_out> and the fly back is on <fly_back>
    And The number of passengers is <adults> adults and <childrens> children
    Then There should be a flight out with departure time "<out_time>"
    And A flight back with departure time "<back_time>"

    Examples:
      | departure_airport | arrival_airport | fly_out    | fly_back   | adults | childrens | out_time | back_time |
      | Madrid            | Eindhoven       | 22/08/2017 | 27/08/2017 | 2      | 1         | 11:20    | 17:00     |
      | Valladolid        | Barcelona       | 03/04/2017 | 09/04/2017 | 2      | 0         | 17:15    | 15:20     |